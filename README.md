# Game Launcher

> Custom menu front end for gaming

A desktop application with a customisable menu system, to run console/arcade emulators and ROMs.
Written in Node.js with the Electron framework.

![Top Menu](readme-docs/topmenu.gif "Top Menu")

# Features

- Gamepad, keyboard and mouse support.
- Load an emulator with a ROM

![Load a ROM](readme-docs/loadgame.gif "Load a ROM")

- Load emulator on its own

![Emulator Only](readme-docs/prognofiles.PNG "Emulator Only")

- Artwork picked up from ROM directory, falls back on default for console, or program default.
- Add a new program with multiple directories for its ROM locations.

![Add](readme-docs/add.png "Add")

- Delete a program from the configuration.

![Remove](readme-docs/remove.png "Remove")

- Config is stored in config.json file.

# Installation

- [npm](https://www.npmjs.com/get-npm)
- [Electron](https://www.christianengvall.se/install-electron-tutorial-app/)
- [Electron Packager](https://www.npmjs.com/package/electron-packager)
- [jQuery](https://www.npmjs.com/package/jquery)
- [form-serialize](https://www.npmjs.com/package/form-serialize)
- [Visual Studio Code](https://code.visualstudio.com/)

# Meta

Dan West - danwest2805@gmail.com

Distributed under the MIT license. See [LICENSE.txt](https://gitlab.com/danwest/game-launcher/blob/master/LICENSE.txt) for more information.

https://gitlab.com/danwest/game-launcher/
