const electron = require("electron");
const { ipcRenderer } = electron;
const exec = require("child_process").execFile;
const configPath = "./resources/config.json";
const fs = require("fs");
let config = JSON.parse(fs.readFileSync(configPath)); // not const - may need to update later
const imgDir = "resources/img/";
const globalFileImg = "default.png";
const menuSpeed = 300;
const fileImageSize = 250;
let loaderFocus = true; // our program has focus

// jQuery isn't defined (globally in the window)
window.$ = window.jQuery = require("jquery");

$(document).ready(function() {
  console.log("ready!");
  console.log("DOMContentLoaded function called");
  /**
   * Add a program to the view and save to config file
   */
  ipcRenderer.on("item:add", function(e, item) {
    // add to view
    addProgramElement(item);
    checkProgramCount();
    // reload, edit and save the file
    JSON.parse(fs.readFileSync(configPath));
    config.programs.push(item);
    const data = JSON.stringify(config, null, 2);
    fs.writeFileSync(configPath, data);
  });

  /**
   * Remove a program from the view and save to config file
   */
  ipcRenderer.on("item:remove", function(e, item) {
    // remove from view
    removeProgramElements(item);
    // reload, edit and save the file
    config = JSON.parse(fs.readFileSync(configPath));
    let remainingProgs = [];
    for (let i = 0; i < config.programs.length; i++) {
      if (!item.sel.includes(config.programs[i].name)) {
        remainingProgs.push(config.programs[i]);
      }
    }
    config.programs = remainingProgs;
    const data = JSON.stringify(config, null, 2);
    fs.writeFileSync(configPath, data);

    // click first program to give clean view
    clickFirstProgram();
  });

  /**
   * Add program buttons to main window
   */
  for (var p = 0; p < config.programs.length; p++) {
    addProgramElement(config.programs[p]);
  }

  checkProgramCount();

  document.addEventListener("keydown", function(event) {
    const buttonFocus = $(".button-focus");
    const keyPressed = event.which;
    const mappedKeys = [37, 38, 39, 40, 13];

    if (mappedKeys.includes(keyPressed)) {
      if (buttonFocus.length === 0) {
        // no program highlighted
        // highlight the first program and return
        clickFirstProgram();
        return;
      }

      if (keyPressed === 38) {
        moveUp();
      } else if (keyPressed === 40) {
        moveDown();
      } else if (keyPressed === 37) {
        moveButtons(false);
      } else if (keyPressed === 39) {
        moveButtons(true);
      } else if (keyPressed === 13) {
        if (buttonFocus.length === 1) {
          buttonFocus
            .first()
            .children()
            .first()
            .trigger("click");
        }
      }
    }

    setFileImageOrientation();
  });
});

let openProgram = exeToRun => {
  exec(exeToRun, function(err, data) {
    if (typeof err != "undefined" && err !== null) {
      alert(err);
    }
    // put focus back on the loader after the program has closed
    loaderFocus = true;
  });
};

let openProgramWithFile = (exeToRun, args) => {
  if (typeof args !== "undefined") {
    if (args !== "") {
      exec(exeToRun, [args], function(err, data) {
        if (typeof err != "undefined" && err !== null) {
          alert(err);
        }
        loaderFocus = true;
      });
    } else if (args === "") {
      openProgram(exeToRun);
    }
  }
};

function moveUp() {
  const prog2 = $(".prog-2");
  const progDiv = $("#program-div");
  const progFilesDiv = $("#program-files-div");

  if (progFilesDiv.hasClass("menu-focus")) {
    progDiv.addClass("menu-focus");
    progFilesDiv.removeClass("menu-focus");

    if (prog2.length === 1) {
      $(".button-focus").removeClass("button-focus");
      prog2.addClass("button-focus");
    } else {
      clickFirstProgram();
    }
  }
}

function moveDown() {
  const progDiv = $("#program-div");
  const progFilesDiv = $("#program-files-div");
  if (progDiv.hasClass("menu-focus")) {
    const filesDivName =
      $(".button-focus")
        .data("name")
        .replace(/ /g, "-")
        .toLowerCase() + "-files";
    const filesDiv = $("#" + filesDivName);
    progDiv.removeClass("menu-focus");
    progFilesDiv.addClass("menu-focus");
    $(".button-focus").removeClass("button-focus");
    // FIND THE RIGHT files-div

    let buttonFocus = filesDiv.find(".file-button").first();

    buttonFocus.addClass("button-focus");
  }
}

function moveButtons(next) {
  const buttonFocus = $(".button-focus");
  let nextButton;

  if (next && buttonFocus.next().length === 1) {
    nextButton = buttonFocus.next();
  } else if (!next && buttonFocus.prev().length === 1) {
    nextButton = buttonFocus.prev();
  } else {
    return;
  }

  const progDiv = $("#program-div");
  if (typeof nextButton !== "undefined") {
    $(".button-focus").removeClass("button-focus");
    nextButton.addClass("button-focus");
    // if top menu, click
    if (progDiv.hasClass("menu-focus")) {
      if (nextButton.data("hasfiles")) {
        nextButton.trigger("click");
      } else {
        // hide any visible files
        $(".files-div").hide(menuSpeed);
      }
    }
  }
}

function addProgramElement(programObj) {
  const progDiv = document.getElementById("program-div");
  const newDiv = document.createElement("div");
  const newImg = document.createElement("img");
  const fileNameDiv = document.createElement("div");
  const textNode = document.createTextNode(programObj.name);
  newDiv.classList.add(
    // "col",
    // "s2",
    "waves-effect",
    "program-button",
    "prog-deselected"
  );
  newImg.src = programObj.hasOwnProperty("img")
    ? imgDir + programObj.img
    : imgDir + "default-program.png";

  fileNameDiv.classList.add("title-text");
  fileNameDiv.appendChild(textNode);

  newDiv.appendChild(newImg);
  newDiv.appendChild(fileNameDiv);
  newDiv.dataset.exe = programObj.exe;
  if (programObj.hasOwnProperty("defaultfileimg")) {
    newDiv.dataset.defaultfileimg = programObj.defaultfileimg;
  }
  newDiv.dataset.name = programObj.name;
  newDiv.id = programObj.name.replace(/ /g, "-").toLowerCase() + "-program";

  progDiv.appendChild(newDiv);

  if (programObj.hasOwnProperty("files") && programObj.files.length > 0) {
    newDiv.dataset.hasfiles = true;
    createFilesDiv(programObj, newDiv);
  } else {
    newDiv.dataset.hasfiles = false;
    newDiv.addEventListener("click", function() {
      $(".files-div").hide(menuSpeed);
      $(".prog-2")
        .removeClass("prog-2")
        .addClass("prog-deselected");
      $(this)
        .removeClass("prog-deselected")
        .addClass("prog-2");

      openProgram(this.dataset.exe);
    });
  }
}

function removeProgramElements(progArray) {
  for (let i = 0; i < progArray.sel.length; i++) {
    let progId = progArray.sel[i].replace(/ /g, "-").toLowerCase() + "-program";
    let filesId = progArray.sel[i].replace(/ /g, "-").toLowerCase() + "-files";
    $("#" + progId).remove();
    $("#" + filesId).remove();
  }

  checkProgramCount();
}

//TODO: IF NONE LEFT, SHOW An 'add' button only
function checkProgramCount() {
  const addProgDiv = $("#add-prog-div");
  if ($(".program-button").length === 0) {
    addProgDiv.show();
  } else {
    addProgDiv.hide();
  }
}

function createFilesDiv(programObj, newDiv) {
  // create a hidden div for this program
  const filesDiv = document.createElement("div");
  filesDiv.id = programObj.name.replace(/ /g, "-").toLowerCase() + "-files";
  filesDiv.classList.add("files-div");
  filesDiv.style.display = "none";

  // for each directory
  for (var f = 0; f < programObj.files.length; f++) {
    // create a div with dataset and add to main div
    const dir = programObj.files[f];

    fs.readdir(programObj.files[f], function(err, items) {
      // each file
      for (var i = 0; i < items.length; i++) {
        if (items[i].endsWith("." + programObj.filetype)) {
          const fileDiv = document.createElement("div");
          fileDiv.classList.add("waves-effect", "file-button");
          fileDiv.dataset.path = dir;

          const fileImg = document.createElement("img");
          let fileImgPath = programObj.defaultfileimg
            ? programObj.defaultfileimg
            : globalFileImg;

          //TODO: look for jpg or png
          const fileWithoutExtension =
            dir +
            items[i]
              .split(".")
              .slice(0, -1)
              .join(".");

          //REFACTOR!!!
          fs.access(fileWithoutExtension + ".png", fs.constants.F_OK, err => {
            fileImg.src = err
              ? imgDir + fileImgPath
              : fileWithoutExtension + ".png";

            if (err) {
              fs.access(
                fileWithoutExtension + ".jpg",
                fs.constants.F_OK,
                err => {
                  fileImg.src = err
                    ? imgDir + fileImgPath
                    : fileWithoutExtension + ".jpg";
                }
              );
            }
          });

          const fileNameDiv = document.createElement("div");
          const textNode = document.createTextNode(
            items[i]
              .split(".")
              .slice(0, -1)
              .join(".")
          );

          fileNameDiv.classList.add("title-text");
          fileNameDiv.appendChild(textNode);

          const commands = programObj.commands.replace(/%f/g, dir + items[i]);

          fileImg.dataset.exec = programObj.exe;
          fileImg.dataset.args = commands;

          fileImg.classList.add("file-img");
          fileImg.addEventListener("click", function() {
            $(".button-focus").removeClass("button-focus");
            //TODO: PARENT FOCUS!!!
            this.classList.add("button-focus");

            openProgramWithFile(this.dataset.exec, fileImg.dataset.args);
          });
          fileDiv.appendChild(fileImg);
          fileDiv.appendChild(fileNameDiv);
          filesDiv.appendChild(fileDiv);
        }
      }
    });
  }

  const programFilesDiv = document.getElementById("program-files-div");
  programFilesDiv.appendChild(filesDiv);

  newDiv.addEventListener("click", function() {
    const progName = this.dataset.name;
    const filesDivId =
      "#" + progName.replace(/ /g, "-").toLowerCase() + "-files";

    $(".prog-2")
      .removeClass("prog-2")
      .addClass("prog-deselected");
    $(this)
      .removeClass("prog-deselected")
      .addClass("prog-2");

    $(".files-div").hide(menuSpeed);
    $(filesDivId).show(menuSpeed);
  });
}

function clickFirstProgram() {
  const programButtons = $(".program-button");
  if (programButtons.length > 0) {
    $(".button-focus").removeClass("button-focus");
    programButtons.first().trigger("click");
    programButtons.first().addClass("button-focus");
  }
}

function setFileImageOrientation() {
  const fileButtonImgs = $(".file-button img");

  $.each(fileButtonImgs, function() {
    const natWidth = $(this).prop("naturalWidth");
    const natHeight = $(this).prop("naturalHeight");

    if (natHeight > natWidth) {
      // portrait
      $(this).prop("height", fileImageSize);
    } else {
      // landscape
      $(this).prop("width", fileImageSize);
    }
  });
}
