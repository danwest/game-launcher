var interval;
var buttonState = [false, false, false, false, false];

window.addEventListener("gamepadconnected", function(e) {
  console.log(
    "Gamepad connected at index %d: %s. %d buttons, %d axes.",
    e.gamepad.index,
    e.gamepad.id,
    e.gamepad.buttons.length,
    e.gamepad.axes.length
  );
});

window.addEventListener("gamepaddisconnected", function(e) {
  console.log(
    "Gamepad disconnected from index %d: %s",
    e.gamepad.index,
    e.gamepad.id
  );
  clearInterval(interval);
});

if (!("ongamepadconnected" in window)) {
  // No gamepad events available, poll instead.
  interval = setInterval(pollGamepads, 75);
}

function buttonPressed(b) {
  if (typeof b == "object") {
    return b.pressed;
  }
  return false;
}

function pollGamepads() {
  var gamepads = navigator.getGamepads
    ? navigator.getGamepads()
    : navigator.webkitGetGamepads
    ? navigator.webkitGetGamepads
    : [];
  for (var i = 0; i < gamepads.length; i++) {
    var gp = gamepads[i];
    if (gp) {
      gamepadLoop();
    }
  }
}

function gamepadLoop() {
  var gamepads = navigator.getGamepads
    ? navigator.getGamepads()
    : navigator.webkitGetGamepads
    ? navigator.webkitGetGamepads
    : [];
  if (!gamepads || !loaderFocus) {
    return;
  }

  const buttonFocus = $(".button-focus");
  var gp = gamepads[0];

  if (buttonFocus.length === 0) {
    // no program highlighted
    // highlight the first program and return
    clickFirstProgram();
    return;
  }

  // Xbox A button is pressed, and wasn't last time we checked
  if (buttonPressed(gp.buttons[0])) {
    if (!buttonState[0]) {
      buttonState[0] = true;
      loaderFocus = false;
      if (buttonFocus.length === 1) {
        buttonFocus
          .first()
          .children()
          .first()
          .trigger("click");
      }
    }
  } else {
    buttonState[0] = false;
  }

  if (gp.axes[1] < -0.5 || buttonPressed(gp.buttons[12])) {
    if (!buttonState[1]) {
      moveUp(); // up
      buttonState[1] = true;
    }
  } else {
    buttonState[1] = false;
  }

  if (gp.axes[1] > 0.5 || buttonPressed(gp.buttons[13])) {
    if (!buttonState[2]) {
      moveDown(); // down
      buttonState[2] = true;
    }
  } else {
    buttonState[2] = false;
  }

  if (gp.axes[0] < -0.5 || buttonPressed(gp.buttons[14])) {
    if (!buttonState[3]) {
      moveButtons(false); // left
      buttonState[3] = true;
    }
  } else {
    buttonState[3] = false;
  }

  if (gp.axes[0] > 0.5 || buttonPressed(gp.buttons[15])) {
    if (!buttonState[4]) {
      moveButtons(true); // right
      buttonState[4] = true;
    }
  } else {
    buttonState[4] = false;
  }
}
