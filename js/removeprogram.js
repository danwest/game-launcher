const electron = require("electron");
const serialize = require("form-serialize");
const { ipcRenderer } = electron;
const form = document.querySelector("#remove-form");
const cancelBtn = document.querySelector("#cancel-btn");
const fs = require("fs");
const configPath = "./resources/config.json";
let config = JSON.parse(fs.readFileSync(configPath));
// jQuery isn't defined (globally in the window)
window.$ = window.jQuery = require("jquery");

let submitForm = e => {
  e.preventDefault();
  // obj -> { foo: 'bar' }
  const obj = serialize(form, { hash: true });
  ipcRenderer.send("item:remove", obj);
};

let closeAddWindow = () => {
  window.close();
};

form.addEventListener("submit", submitForm);
cancelBtn.addEventListener("click", () => {
  closeAddWindow();
});

// if we have 1 or more programs loaded, create the element
if (config.programs.length > 0) {
  let selectElem = $('<select multiple="multiple" name="sel"></select>');
  for (let i = 0; i < config.programs.length; i++) {
    const progOpt = new Option(
      config.programs[i].name,
      config.programs[i].name
    );
    selectElem.append(progOpt);
  }
  $("#prog-list-div").append(selectElem);
}
