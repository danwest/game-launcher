const electron = require("electron");
const serialize = require("form-serialize");
const { ipcRenderer } = electron;
const form = document.querySelector("#prog-form");
const cancelBtn = document.querySelector("#cancel-btn");

let submitForm = e => {
  e.preventDefault();
  // obj -> { foo: 'bar' }
  const obj = serialize(form, { hash: true });
  ipcRenderer.send("item:add", obj);
};

let closeAddWindow = () => {
  window.close();
};

form.addEventListener("submit", submitForm);
cancelBtn.addEventListener("click", () => {
  closeAddWindow();
});
