const electron = require("electron");
const url = require("url");
const path = require("path");
const { app, BrowserWindow, Menu, ipcMain } = electron;

// set environment to hide dev tools for prod build
//process.env.NODE_ENV = "production";

let launcherWindow;
let addProgramWindow;

// Listen for the app to be ready
app.on("ready", () => {
  // create new window
  launcherWindow = new BrowserWindow({
    fullscreen: true,
    backgroundColor: "#2e2c29"
  });
  // Load html into window
  launcherWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, "launcher.html"),
      protocol: "file:",
      slashes: true
    })
  );

  // Quit app when closed
  launcherWindow.on("closed", () => {
    app.quit();
  });

  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  // Insert menu
  Menu.setApplicationMenu(mainMenu);
});

// Handle create add window
let createAddProgramWindow = () => {
  // create new window
  addProgramWindow = new BrowserWindow({
    width: 725,
    height: 528,
    title: "Add Program",
    frame: false,
    parent: launcherWindow,
    modal: true
  });
  // Load html into window
  addProgramWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, "addprogram.html"),
      protocol: "file:",
      slashes: true
    })
  );

  // Garbage collection handle
  addProgramWindow.on("close", () => {
    addProgramWindow = null;
  });
};

// Handle create add window
let createRemoveProgramWindow = () => {
  // create new window
  removeProgramWindow = new BrowserWindow({
    width: 500,
    height: 500,
    title: "Remove Program",
    frame: false,
    parent: launcherWindow,
    modal: true
  });
  // Load html into window
  removeProgramWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, "removeprogram.html"),
      protocol: "file:",
      slashes: true
    })
  );

  // Garbage collection handle
  removeProgramWindow.on("close", () => {
    removeProgramWindow = null;
  });
};

// Catch item:add
ipcMain.on("item:add", (e, item) => {
  launcherWindow.webContents.send("item:add", item);
  addProgramWindow.close();
});

// Catch item:remove
ipcMain.on("item:remove", (e, item) => {
  launcherWindow.webContents.send("item:remove", item);
  removeProgramWindow.close();
});

// Create menu template (const makes binding immutable, not value)
const mainMenuTemplate = [
  {
    label: "File",
    submenu: [
      {
        label: "Add Program...",
        click() {
          createAddProgramWindow();
        }
      },
      {
        label: "Remove Program...",
        click() {
          createRemoveProgramWindow();
        }
      },
      {
        label: "Quit",
        accelerator: process.platform == "win32" ? "Ctrl+Q" : "Command+Q",
        click() {
          app.quit();
        }
      }
    ]
  }
];

// If mac, add empty object to menu
if (process.platform == "darwin") {
  mainMenuTemplate.unshift({});
}

// Add dev tools if not prod
if (process.env.NODE_ENV !== "production") {
  mainMenuTemplate.push({
    label: "Developer Tools",
    submenu: [
      {
        label: "Toggle DevTools",
        accelerator: process.platform == "win32" ? "Ctrl+I" : "Command+I",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        }
      },
      {
        role: "reload"
      }
    ]
  });
}
